﻿using System;

namespace MinMax
{
    public class StrategoGrid
    {
        private Players[,] gameGrid;


        public StrategoGrid(int gridSize)
        {
            if(gridSize < 4)
                throw new Exception("Grid is too small");
            gameGrid = new Players[gridSize,gridSize];
        }
        
        
    }
}