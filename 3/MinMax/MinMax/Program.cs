﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using Terminal.Gui;

namespace MinMax
{
    class Program
    {
        static void Main(string[] args)
        {
            new StrategoGrid(5);
//            var test = is_win("XXX------");
            Application.Init ();
            var top = Application.Top;

            // Creates the top-level window to show
            var win = new Window (new Rect (0, 1, top.Frame.Width, top.Frame.Height-1), "Stratego");
            top.Add (win);
            Application.Run ();
        }


        public static bool is_win(string board)
        {

            //check rows
            for (var i = 0; i < 3; i++)
            {
                if (board[i * 3] == board[i * 3 + 1] && board[i * 3 + 1] == board[i * 3 + 2] && board[i * 3] != '-')
                    return true;
            }

            //cols
            for (var i = 0; i < 3; i++)
            {
                if (board[i] == board[i + 3] && board[i] == board[i + 6] && board[i] != '-')
                    return true;
            }

            //diagonal
            if (board[0] == board[4] && board[4] == board[8] && board[4] != '-')
                return true;
            if (board[2] == board[4] && board[4] == board[6] && board[4] != '-')
                return true;

            return false;
        }



        public static (int, int) minmax(string board, char player, int depth)
        {
            var next_player = player == 'O' ? 'X' : 'O';
            if (is_win(board))
            {
                if (player == 'X')
                    return (depth - 10, -1);
                else
                    return (10 - depth,-1);
            }

            depth++;

            var result_list = new List<(int,int)>();
            var possible_moves_count = board.Count(s => s == '-');
            if (possible_moves_count == 0)
                return (0, -1);

            var possible_moves_idx = new List<int>();
            
            for(var i =0; i < board.Length; i++)
            {
                if(board[i] == '-')
                    possible_moves_idx.Add(i);
                
            }
            
            foreach(var i in possible_moves_idx)
            {
                StringBuilder strB = new StringBuilder(board);
                strB[i] = player;
                board = strB.ToString();
                var result = minmax(board, next_player, depth);
                result_list.Add(result);
                
                strB = new StringBuilder(board);
                strB[i] = '-';
                board = strB.ToString();
            }

            if (player == 'X')
            {
                var max_elem = max(result_list);
                return (max_elem.Item1, possible_moves_idx[result_list.IndexOf(max_elem)]);
            }
            else
            {
                var min_elem = min(result_list);
                return (min_elem.Item1, possible_moves_idx[result_list.IndexOf(min_elem)]);

            }
        }

        public static (int, int) max(List<(int, int)> list)
        {
            var max = list[0];

            foreach (var elem in list)
                if (elem.Item1 > max.Item1)
                    max = elem;
            return max;
        }
        public static (int, int) min(List<(int, int)> list)
        {
            var max = list[0];

            foreach (var elem in list)
                if (elem.Item1 < max.Item1)
                    max = elem;
            return max;
        }
    }
}