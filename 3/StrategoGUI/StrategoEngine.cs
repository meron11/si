﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SharpDX;

namespace StrategoGUI
{
    public class StrategoEngine
    {
        public static int CalcPoints(List<GameField> gameBoard, int gridEdgeSize)
        {
            var points = 0;
            
            for (var i = 0; i < gridEdgeSize; i++)
            {
                points += calcRow(i);
                points += calcCol(i);
            }

            //diagonals piece of art ;)
            var toLeftDiagonals = Enumerable.Range(1, gridEdgeSize-1).ToList();
            var toRightDiagonals = Enumerable.Range(0, gridEdgeSize-1).Reverse().ToList();

            var startingIndex = 1 * gridEdgeSize - 1;
            
            for (var i = 0; i < gridEdgeSize - 2; i++)
            {
                toRightDiagonals.Add(startingIndex + 1);
                toLeftDiagonals.Add(toRightDiagonals.Last() + gridEdgeSize - 1);
                startingIndex = toLeftDiagonals.Last();
            }

            var diagonalSize = 1;
            
            foreach (var index in toLeftDiagonals)
            {
                var pkt = 0;
                var idx = index;
                diagonalSize = index >= gridEdgeSize ? diagonalSize - 1 : diagonalSize + 1;
                for(var i =0; i < diagonalSize; i++)
                {
                    if(gameBoard[idx] is Nobody)
                    {
                        pkt = 0;
                        break;
                    }
                    pkt++;
                    idx += gridEdgeSize - 1;
                }   
                points += pkt;
            }
            
            toRightDiagonals.Reverse();
            diagonalSize = 1;
            var increase = true;
            foreach (var index in toRightDiagonals)
            {
                var pkt = 0;
                var idx = index;
                diagonalSize = increase ? diagonalSize + 1 : diagonalSize - 1;
                for(var i =0; i < diagonalSize; i++)
                {
                    if(gameBoard[idx] is Nobody)
                    {
                        pkt = 0;
                        break;
                    }
                    pkt++;
                    idx += gridEdgeSize + 1;
                }
                points += pkt;
                if (index == 0)
                    increase = false;
            }

            
            int calcRow(int row)
            {
                var startIdx = row * gridEdgeSize;
                var pkt = 0;
                
                for (var i = 0; i < gridEdgeSize; i++)
                {
                    if (gameBoard[startIdx++] is Nobody)
                        return 0;
                    pkt++;
                }

                return pkt;
            }
            
            int calcCol(int col)
            {
                var startIdx = col;
                var pkt = 0;
                for (var i = 0; i < gridEdgeSize; i++)
                {
                    if (gameBoard[startIdx] is Nobody)
                        return 0;
                    pkt++;
                    startIdx += gridEdgeSize;
                }

                return pkt;
            }
            
            return points;
        }
        
        public static int GetMinMaxMove(List<GameField> board, bool isMaximisingPlayer, int gridEdgeSize, int maxDepth = 3) {

            //root iteration ;) 
            var newGameMoves =  FindPosssibleMovesIndexes(board);
            var bestMove = int.MinValue;
            var bestMoveFound = -1;

            for(var i = 0; i < newGameMoves.Count; i++) {
                var newGameMove = newGameMoves[i];
                board[i] = new AI(board[i].Location);
                var value = Minmax(board,1, !isMaximisingPlayer, gridEdgeSize, --maxDepth);
                
                board[i] = new Nobody(board[i].Location); //rollback

                if (value < bestMove) continue;
                bestMove = value;
                bestMoveFound = newGameMove;
            }
            return bestMoveFound;
        }
        
        public static int GetMinMaxAlphaBetaMove(List<GameField> board, bool isMaximisingPlayer, int gridEdgeSize, int maxDepth = 3) {

            //root iteration ;) 
            var newGameMoves =  FindPosssibleMovesIndexes(board);
            var bestMove = int.MinValue;
            var bestMoveFound = -1;

            for(var i = 0; i < newGameMoves.Count; i++) {
                var newGameMove = newGameMoves[i];
                board[i] = new AI(board[i].Location);
                var value = MinmaxAlphaBeta(board,1,int.MinValue,int.MaxValue, !isMaximisingPlayer, gridEdgeSize, maxDepth);
                
                board[i] = new Nobody(board[i].Location); //rollback

                if (value < bestMove) continue;
                bestMove = value;
                bestMoveFound = newGameMove;
            }
            return bestMoveFound;
        }

        private static int Minmax(List<GameField> board,int depth, bool isMaximisingPlayer, int gridEdgeSize, int maxDepth = 3)
        {
            var possible_moves_count = board.FindAll(s => s is Nobody).Count;

            if (maxDepth  <= depth || (possible_moves_count == 0))
            {
                if(isMaximisingPlayer)
                    return depth - CalcPoints(board, gridEdgeSize);
                else
                    return CalcPoints(board, gridEdgeSize) - depth;
            }

            var possible_moves_idx = FindPosssibleMovesIndexes(board);

            //max
            if (isMaximisingPlayer)
            {
                var bestMove = int.MinValue;
                foreach (var i in possible_moves_idx)
                {
                    board[i] = new AI(board[i].Location);
                    bestMove = Math.Max(bestMove,Minmax(board,++depth,false, gridEdgeSize, maxDepth));

                    board[i] = new Nobody(board[i].Location); //rollback
                }

                return bestMove;
            }
            else // min
            {
                var bestMove = int.MaxValue;
                foreach (var i in possible_moves_idx)
                {
                    board[i] = new Player(board[i].Location);
                    bestMove = Math.Min(bestMove,Minmax(board,++depth,true, gridEdgeSize, maxDepth));

                    board[i] = new Nobody(board[i].Location); //rollback
                }

                return bestMove;
            }
        }
        
        private static int MinmaxAlphaBeta(List<GameField> board,int depth, int alpha, int beta, bool isMaximisingPlayer, int gridEdgeSize, int maxDepth = 3)
        {
            var possible_moves_count = board.FindAll(s => s is Nobody).Count;

            if (maxDepth <= depth || (possible_moves_count == 0))
            {
                if (isMaximisingPlayer)
                    return depth - board.Count(i => i is Player);
                else
                    return depth - board.Count(i => i is AI);
            }

            var possible_moves_idx = FindPosssibleMovesIndexes(board);

            //max
            if (isMaximisingPlayer)
            {
                var bestMove = int.MinValue;
                foreach (var i in possible_moves_idx)
                {
                    board[i] = new AI(board[i].Location);
                    bestMove = Math.Max(bestMove,MinmaxAlphaBeta(board,++depth,alpha,beta, false, gridEdgeSize, maxDepth));

                    board[i] = new Nobody(board[i].Location); //rollback
                    
                    alpha = Math.Max(alpha, bestMove);
                    if (beta <= alpha) {
                        return bestMove;
                    }
                    
                }

                return bestMove;
            }
            else // min
            {
                var bestMove = int.MaxValue;
                foreach (var i in possible_moves_idx)
                {
                    board[i] = new Player(board[i].Location);
                    bestMove = Math.Min(bestMove,MinmaxAlphaBeta(board,++depth,alpha,beta,true, gridEdgeSize, maxDepth));
                    
                    beta = Math.Min(beta, bestMove);
                    if (beta <= alpha) {
                        return bestMove;
                    }
                    board[i] = new Nobody(board[i].Location); //rollback
                }

                return bestMove;
            }
        }

        private static List<int> FindPosssibleMovesIndexes(List<GameField> board)
        {
            var possible_moves_idx = new List<int>();

            for (var i = 0; i < board.Count; i++)
            {
                if (board[i] is Nobody)
                    possible_moves_idx.Add(i);
            }

            return possible_moves_idx;
        }

   
    }
}