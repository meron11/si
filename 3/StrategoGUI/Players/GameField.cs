﻿namespace StrategoGUI
{
    public abstract  class GameField
    {
        public CellLocation Location { get; set; }
        public GameField(CellLocation location)
        {
            Location = location;
        }
    }
}