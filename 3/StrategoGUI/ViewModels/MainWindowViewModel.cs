﻿using System.Reactive;
using ReactiveUI;


namespace StrategoGUI.ViewModels
{
    public class MainWindowViewModel: ReactiveObject
    {
        private int _boardSize = 4;
        private int _maxDepth = 2;
        private bool _alphaBeta = false;
        private StrategoEngine _gameBoard = null;

        public MainWindowViewModel()
        {
            StartGameCommand = ReactiveCommand.Create(StartTheGame);
        }

        public ReactiveCommand StartGameCommand { get; }



        private void StartTheGame()
        {
            var window = new GameWindow();
            window.DataContext = new GameWindowViewModel(BoardSize, MaxDepth, AlphaBeta);
            window.Show();
        }
        
        public int BoardSize
        {
            get { return _boardSize; }
            set { this.RaiseAndSetIfChanged(ref _boardSize, value); }
        }
        
        public int MaxDepth
        {
            get { return _maxDepth; }
            set { this.RaiseAndSetIfChanged(ref _maxDepth, value); }
        }
        
        
        public bool AlphaBeta
        {
            get { return _alphaBeta; }
            set { this.RaiseAndSetIfChanged(ref _alphaBeta, value); }
        }
    }
}