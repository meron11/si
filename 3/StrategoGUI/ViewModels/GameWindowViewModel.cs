﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive;
using ReactiveUI;


namespace StrategoGUI.ViewModels
{
    public class GameWindowViewModel : ReactiveObject
    {
        public ReactiveList<GameField> GameFields { get; set; }

        private int gridEdgeSize = 0;
        private int _maxDepth = 2;

        private int _width;

        public int Width
        {
            get { return _width; }
            set { this.RaiseAndSetIfChanged(ref _width, value); }
        }
        
        private long _lastMoveTime = 0;

        public long LastMoveTime
        {
            get { return _lastMoveTime; }
            set { this.RaiseAndSetIfChanged(ref _lastMoveTime, value); }
        }

        
        public int Height
        {
            get { return _width + 150; }
        }
        
        private int _playerPoints = 0;

        public int PlayerPoints
        {
            get { return _playerPoints; }
            set { this.RaiseAndSetIfChanged(ref _playerPoints, value); }
        }
        
        private int _AIPoints = 0;

        public int AIPoints
        {
            get { return _AIPoints; }
            set { this.RaiseAndSetIfChanged(ref _AIPoints, value); }
        }
        
        public ReactiveCommand ClickCellCommand { get; }

        private bool _alphaBeta = false;

        

        public GameWindowViewModel(int gridEdgeSize, int maxDepth, bool alphaBeta)
        {

            this._alphaBeta = alphaBeta;
            this.gridEdgeSize = gridEdgeSize;
            GenerateFields(gridEdgeSize);

            this.Width = gridEdgeSize * 63 + 1;
            this._maxDepth = maxDepth;
            
            ClickCellCommand = ReactiveCommand.Create<GameField>(ClickCell);

        }


        /**
         * When user click empty cell
         */
        private void ClickCell(GameField field)
        {
            var idx = this.GameFields.IndexOf(field);
            this.GameFields[idx] = new Player(this.GameFields[idx].Location);

            //sum minus new
            this.PlayerPoints += Math.Abs((PlayerPoints + AIPoints) - StrategoEngine.CalcPoints(this.GameFields.ToList(), gridEdgeSize));

            if (this.GameFields.Count(i => i is Nobody) != 0)
            {
                int move;
                
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                if(_alphaBeta)
                    move = StrategoEngine.GetMinMaxAlphaBetaMove(this.GameFields.ToList(), true, gridEdgeSize, this._maxDepth);
                else
                    move = StrategoEngine.GetMinMaxMove(this.GameFields.ToList(), true, gridEdgeSize, this._maxDepth);

                stopWatch.Stop();
                Console.WriteLine(stopWatch.ElapsedMilliseconds);
                this.LastMoveTime = stopWatch.ElapsedMilliseconds;
                this.GameFields[move] = new AI(this.GameFields[move].Location);
                this.AIPoints += Math.Abs((PlayerPoints + AIPoints) - StrategoEngine.CalcPoints(this.GameFields.ToList(), gridEdgeSize));
            }
        }

        private void GenerateFields(int gridEdgeSize)
        {
            this.GameFields = new ReactiveList<GameField>();

            var row = 0;
            var column = 0;
            for (var i = 0; i < gridEdgeSize * gridEdgeSize; i++)
            {
                var loc = new CellLocation(x: row, y: column);
                this.GameFields.Add(new Nobody(loc));

                row += 63;
                if (i != 0 && (i + 1) % gridEdgeSize == 0)
                {
                    row = 0;
                    column += 63;
                }
            }
        }
    }
}