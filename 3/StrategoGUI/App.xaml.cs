using Avalonia;
using Avalonia.Markup.Xaml;

namespace StrategoGUI
{
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }
   }
}