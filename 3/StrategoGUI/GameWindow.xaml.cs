using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace StrategoGUI
{
    public class GameWindow : Window
    {
        public GameWindow()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}