﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.ComTypes;

namespace Lab_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] flowMatrix;
            int[,] distanceMatrix;
            string[] buff = File.ReadAllLines("DataSets/had16.dat");

            int n_facilities = Convert.ToInt32(buff[0].Trim());
            distanceMatrix = readArrayFromStringBuff(2,n_facilities,buff);
            flowMatrix = readArrayFromStringBuff(3+n_facilities,n_facilities,buff);
            
            int populationSize = 100;
            double crossover_propability = 0.80;
            double mutation_probability = 0.002;
            var gen = 200;
            var tournamentSelection = false;
            var selectionType = tournamentSelection ? "Tour" : "Roulette";

            var population_list = GenerateRandomPopulation(n_facilities, populationSize);

            FormattableString header = $"Pop={populationSize}/Epochs={gen}/Pcross={crossover_propability}/Pmut={mutation_probability}/{selectionType}";
            Console.WriteLine(header.ToString(new CultureInfo("en-US")));
            Console.WriteLine("Epoch,Avg Fitness,Best Fitness,Worse fitness");
            
            for (var i = 0; i < gen; i++)
            {
                var fit_scores = FitnessScore(population_list, flowMatrix, distanceMatrix);
                var fit_scores_norm = NormaliseFintessScore(fit_scores);
                List<List<int>> selected_ch;
                if(tournamentSelection)
                    selected_ch = TournamentSelection(population_list, fit_scores_norm, false);
                else
                    selected_ch = RouletteSelection(population_list, fit_scores_norm, false);
                var crossed_ch = CrossoverPopulation(selected_ch, crossover_propability);
                var mutated_ch = MutationPopulation(crossed_ch, mutation_probability);
                var max_fitness = fit_scores.Max();
                var min_fitness = fit_scores.Min();
                var max_chromosome = population_list[fit_scores.IndexOf(fit_scores.Max())];
                population_list = crossed_ch;
                
                FormattableString formattableString = $"{i},{1.0 / fit_scores.Average()},{1.0 / max_fitness},{1.0 / min_fitness}";
                
                Console.WriteLine(formattableString.ToString(new CultureInfo("en-US")));
            }

        }



        public static List<List<int>> GenerateRandomPopulation(int n_facilities, int n_chromosomes)
        {
            var buff = new List<List<int>>();
            var rnd = new Random();
            for (int i = 0; i < n_chromosomes; i++)
                buff.Add(Enumerable.Range(0, n_facilities).OrderBy(item => rnd.Next()).ToList());
            
            return buff;
        }


        public static List<double> FitnessScore(List<List<int>> population_list, int[,] flow_matrix, int[,] distance_matrix)
        {
            var n_facilities = population_list[0].Count;
            
            var fitness_scores_list = new List<double>();

            foreach (var chromosome_list in population_list)
            {
                var chromosome_fitness = CalcChromosomeFitness(flow_matrix, distance_matrix, n_facilities, chromosome_list);

                fitness_scores_list.Add(1.0 / (chromosome_fitness / 2.0) );
            }

            return fitness_scores_list;
        }

        private static int CalcChromosomeFitness(int[,] flow_matrix, int[,] distance_matrix, int n_facilities,
            List<int> chromosome_list)
        {
            var chromosome_fitness = 0;
            for (var firstFacilityIndex = 0; firstFacilityIndex < n_facilities; firstFacilityIndex++)
            {
                var firstFacility = chromosome_list[firstFacilityIndex];
                for (int secondFacilityIndex = 0; secondFacilityIndex < n_facilities; secondFacilityIndex++)
                {
                    var secondFacility = chromosome_list[secondFacilityIndex];
                    var fitness = flow_matrix[firstFacility, secondFacility] *
                                  distance_matrix[firstFacilityIndex, secondFacilityIndex];
                    chromosome_fitness += fitness;
                }
            }

            return chromosome_fitness;
        }

        public static List<double> NormaliseFintessScore(List<double> fitness_score)
        {
            var buff = new List<double>();
            var fitnessSum = fitness_score.Sum();
            fitness_score.ForEach(x => buff.Add(x/fitnessSum));
            return buff;
        }

        //todo valid
        public static List<List<int>> RouletteSelection(List<List<int>> population_list, List<double> fitness_scores_list,
            bool elitism = true)
        {
            var population_size = elitism ? fitness_scores_list.Count - 1 : fitness_scores_list.Count;         
            List<List<int>> new_species = new List<List<int>>();           
            
            var index_pop_fit_cumsum = new List<MutableTupleForRoulette>();
            for(var i=0;i < population_size;i++){
                var tuple = new MutableTupleForRoulette(i,population_list[i],fitness_scores_list[i],0);
                index_pop_fit_cumsum.Add(tuple);
            }
            index_pop_fit_cumsum = index_pop_fit_cumsum.OrderByDescending(x => x.Fitness).ToList();

            double aux = 0;
            double sum =  fitness_scores_list.Sum();

            for(int i = 0; i < population_size; i++){
                var tournament = new Random().NextDouble();

                for (var j = 0; j < population_size; j++)
                {
                    aux += index_pop_fit_cumsum[j].Fitness/ sum;
                    if (tournament <= aux)
                    {
                        new_species.Add(index_pop_fit_cumsum[j].Population);
                        break;
                    }
                }
            }
            return new_species;           
        }

        public static List<List<int>> TournamentSelection(List<List<int>> population_list, List<double> fitness_scores_list,
            bool elitism = true)
        {
            List<List<int>> new_species = new List<List<int>>();
            var population_size = elitism ? fitness_scores_list.Count - 1 : fitness_scores_list.Count;

            Random rand = new Random();
            for (var i = 0; i < population_size; i++)
            {
                var of_parent_idx = rand.Next(0, fitness_scores_list.Count);
                var tf_parent_idx = rand.Next(0, fitness_scores_list.Count);

                var winner = population_list[tf_parent_idx];
                if (fitness_scores_list[of_parent_idx] > fitness_scores_list[tf_parent_idx])
                    winner = population_list[of_parent_idx];
              
                new_species.Add(winner);
            }
            if(elitism)
                new_species.Add(population_list[fitness_scores_list.IndexOf(fitness_scores_list.Max())]);

            return new_species;

        }

        public static Tuple<List<int>,List<int>> ChromosomeCrossover(List<int> chromosome_o, List<int> chromosome_s)
        {
            var chromosomeOutput = new List<int>(chromosome_o);
            var chromosomeSource = new List<int>(chromosome_s);
            var crossoverPoint = new Random().Next(0, chromosome_o.Count);

            for (int chromosomeIndex = 0; chromosomeIndex < crossoverPoint; chromosomeIndex++)
            {
                var facilityOutput = chromosomeOutput[chromosomeIndex];
                var facilitySource = chromosomeSource[chromosomeIndex];

                var facilityOutputIndex = chromosomeOutput.IndexOf(facilitySource);
                var facilitySourceIndex = chromosomeSource.IndexOf(facilityOutput);
                
                chromosomeOutput[facilityOutputIndex] = facilityOutput;
                chromosomeSource[facilitySourceIndex] = facilitySource;
                
                chromosomeOutput[chromosomeIndex] = facilitySource;
                chromosomeSource[chromosomeIndex] = facilityOutput;

            }
            
            return new Tuple<List<int>, List<int>>(chromosomeOutput, chromosomeSource);
        }


        public static List<List<int>> CrossoverPopulation(List<List<int>> new_spieces, double crossover_probality)
        {
            var speciesNotCrossed = new List<List<int>>();
            var speciesToCross = new List<List<int>>();

            //type to cross
            foreach (var n_chrom in new_spieces)
            {
                var rnd = new Random().NextDouble();
                if (rnd < crossover_probality)
                    speciesToCross.Add(n_chrom);
                else
                    speciesNotCrossed.Add(n_chrom);

            }
                     
            
            var crossover_tuples = new List<Tuple<List<int>,List<int>>>();
            while (speciesToCross.Count > 0)
            {
                //get chromosome and remove from list
                List<int> chromosome = speciesToCross[0];
                speciesToCross.RemoveAt(0);

                //exit because last
                if (!(speciesToCross.Count > 0))
                {
                    speciesNotCrossed.Add(chromosome);
                    break;
                }
                //find cross buddy
                List<int> randomCrossBuddy  = speciesToCross[new  Random().Next(speciesToCross.Count)];
                speciesToCross.Remove(randomCrossBuddy);
                crossover_tuples.Add(new Tuple<List<int>,List<int>>(chromosome,randomCrossBuddy));
            }
                
                
            var afterCrossover = new List<List<int>>();
            foreach (var crossoverBuddies in crossover_tuples)
            {
                var afterCrossoverTuple = ChromosomeCrossover(crossoverBuddies.Item1, crossoverBuddies.Item2);
                
                afterCrossover.Add(afterCrossoverTuple.Item1);
                afterCrossover.Add(afterCrossoverTuple.Item2);
            }


            afterCrossover.AddRange(speciesNotCrossed);
            return afterCrossover;
        }

        public static List<List<int>> MutationPopulation(List<List<int>> new_spieces, double mutation_probality)
        {
            var mutated = new List<List<int>>();

            foreach (var chromosome in new_spieces)
            {
                for (var chromosomeIndex = 0; chromosomeIndex < chromosome.Count; chromosomeIndex++)
                {
                    var rnd = new Random().NextDouble();
                    if (rnd <= mutation_probality)
                    {
                        var swapIndex = new Random().Next(0,chromosome.Count);
                        var oldValue = chromosome[chromosomeIndex];
                        chromosome[chromosomeIndex] = chromosome[swapIndex];
                        chromosome[swapIndex] = oldValue;
                    }
                }
                mutated.Add(chromosome);
            }

            return mutated;
        }
        
        
        private static int[,] readArrayFromStringBuff(int startingIndex,int n, string[] buff)
        {
            var buffArray = new int[n,n];
            
            for (int i = startingIndex, index = 0; index < n; i++, index++)
            {
                var buff2 = new List<int>();
                var numbers = buff[i].Trim().Split("  ");
                for (var j = 0; j < n; j++)
                    buffArray[index, j] = Convert.ToInt32(numbers[j]);
            }

            return buffArray;
        }   
        
    }

    public class MutableTupleForRoulette
    {
        public int Index { get; set; }
        public List<int> Population { get; set; }
        public double Fitness { get; set; }
        public double Propability { get; set; }


        public MutableTupleForRoulette(int Index, List<int> Population, double Fitness, double propability){
            this.Index = Index;
            this.Population = Population;
            this.Fitness = Fitness;
            this.Propability = propability;
        }
    }
}