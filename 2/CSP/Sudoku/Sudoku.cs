﻿using System;
using System.Text;

namespace CSP.Sudoku
{
    public class Sudoku
    {
        protected int[,] array;

        public int[,] Array => array;

        public Sudoku(int[,] input)
        {
            this.array = input;
        }


        /// <summary>
        /// Find first empty cell
        /// </summary>
        /// <returns></returns>
        protected Tuple<int, int> FindUnasignedCell()
        {
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                    if (array[i, j] == 0)
                        return new Tuple<int, int>(i, j);
            }

            return new Tuple<int, int>(-1, -1); //if not found
        }


        /// <summary>
        /// check if value is used on 3x3 box
        /// </summary>
        /// <returns></returns>
        protected bool UsedInBox(int startX, int startY, int value)
        {
            for (int row = 0; row < 3; row++)
            for (int col = 0; col < 3; col++)
                if (array[row + startX, col + startY] == value)
                    return true;
            return false;
        }

        protected bool UsedInRow(int x, int value)
        {
            for (var col = 0; col < array.GetLength(1); col++)
            {
                if (array[x, col] == value)
                    return true;
            }

            return false;
        }

        protected bool UsedInCol(int y, int value)
        {
            for (var i = 0; i < array.GetLength(0); i++)
            {
                if (array[i, y] == value)
                    return true;
            }

            return false;
        }

        protected bool CanBePlacedHere(int x, int y, int value)
        {
            // i must check a box 3x3 and row and column
            return !UsedInRow(x, value) &&
                   !UsedInCol(y, value) &&
                   !UsedInBox(x - x % 3, y - y % 3, value);
        }

        public string ToString()
        {
            var buff = new StringBuilder();
            for (int row = 0; row < array.GetLength(0); row++)
            {
                for (int col = 0; col < array.GetLength(1); col++)
                {
                    buff.Append(array[row, col] + " ");
                }

                buff.Append("\n");
            }

            return buff.ToString();
        }
    }
}