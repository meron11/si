﻿using System;
using System.Text;

namespace CSP.Sudoku
{
    public class SudokuBackTracking : Sudoku
    {
        public long iterations=0;
        public bool Solve()
        {
            iterations++;
            var unassignedCell = FindUnasignedCell();
            if (unassignedCell.Item1 == -1 && unassignedCell.Item2 == -1)
                return true;

            for (int a = 1; a <= 9; a++)
            {
                if (CanBePlacedHere(unassignedCell.Item1, unassignedCell.Item2, a))
                {
                    // make tentative assignment
                    array[unassignedCell.Item1,unassignedCell.Item2] = a;
 
                    // return, if success, yay!
                    if (Solve())
                        return true;
 
                    array[unassignedCell.Item1,unassignedCell.Item2] = 0;
                }
            }
            return false;
        }


        public SudokuBackTracking(int[,] input) : base(input)
        {
        }
    }
}