﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace CSP.Sudoku
{
    public class SudokuForwardChecking : Sudoku
    {
        public long iterations = 0;
        private List<List<int>> domain = new List<List<int>>();

        public SudokuForwardChecking(int[,] input) : base(input)
        {
            UpdateDomain();
        }

        public bool Solve()
        {
            iterations++;
            var unassignedCell = FindUnasignedCell();
            var x = unassignedCell.Item1;
            var y = unassignedCell.Item2;
            if (x == -1 && y == -1)
                return true;

            var domainBuff = new List<List<int>>(domain);
            var domX = FindInDomain(x, y);
            foreach (var possibleVal in domX)
            {
                array[x, y] = possibleVal;
                var domainIsValid = UpdateDomain();
                if (domainIsValid && Solve())
                    return true;
                //rollback
                array[x, y] = 0;
                this.domain = domainBuff;

            }
            return false;
        }


        public bool RollbackRequired()
        {
            foreach (var d in domain)
            {
                if (d.Count == 0)
                    return true;
            }
            return false;
        }

        private List<int> FindInDomain(int row, int column)
        {
            var width = array.GetLength(0);
            return domain[(width * row) + column];
        }

        private bool UpdateDomain()
        {
            domain.Clear();

            for (var row = 0; row < array.GetLength(0); row++)
                for (var column = 0; column < array.GetLength(1); column++)
                {
                    var domX = new List<int>();
                    domain.Add(domX);

                    if (array[row, column] != 0) //aleady placed
                    {
                        domX.Add(array[row, column]);
                        continue;
                    }

                    for (var nr = 1; nr < 10; nr++) //check every value
                        if (CanBePlacedHere(row, column, nr))
                            domX.Add(nr);
                    if(domX.Count == 0)
                        return false; //is invalid, not needed more
                }
            return true;
        }
    }
}