﻿namespace CSP.NQueens
{
    public class NQueensBacktracking: NQueens
    {
        public long iterations=0;

        public NQueensBacktracking(int boardSize) : base(boardSize)
        {
        }


        public bool Solve() => Solve(0);

        private bool Solve(int col)
        {
            if (col >= boardSize)
                return true;

            iterations++;
            
            for (int i = 0; i < boardSize; i++)
            {
                if (IsSafe(i, col))
                {
                    array[i,col] = true;
 
                    if ( Solve(col + 1) )
                        return true;
 
                    array[i,col] = false;
                }
            }
 

            return false;
        }
    }
}