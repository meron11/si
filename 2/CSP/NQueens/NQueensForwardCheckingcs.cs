﻿using System.Collections.Generic;

namespace CSP.NQueens
{
    public class NQueensForwardChecking : NQueens
    {
        public long iterations = 0;


        public NQueensForwardChecking(int boardSize) : base(boardSize)
        {
        }



        public bool Solve() => Solve(0);


        private bool Solve(int column)
        {
            if (column == boardSize)
                return true;

            iterations++;
            var rowsProposition = RowsProposition(column);
            foreach (var row in rowsProposition)
            {
                array[row, column] = true;
                if (atLeastOneSolution(column + 1))
                    if (Solve(column + 1))
                        return true;

                array[row, column] = false;
            }
            return false;
        }

        private bool atLeastOneSolution(int column)
        {
            for (int i = column; i < boardSize; i++)
            {
                var rowsProposition = RowsProposition(column);
                if (rowsProposition.Count == 0)
                    return false;
            }
            return true;
        }




        private List<int> RowsProposition(int column)
        {
            var buff = new List<int>();

            for (int i = 0; i < boardSize; i++)
            {
                if (IsSafe(i, column))
                    buff.Add(i);
            }
            return buff;
        }
    }
}