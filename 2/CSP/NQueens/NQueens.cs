using System.Text;

namespace CSP.NQueens{
    public class NQueens 
    {
        protected bool[,] array;

        public bool[,] Array => array;

        protected int boardSize => array.GetLength(0);

        public NQueens(int boardSize)
        {
            array = new bool[boardSize,boardSize];
        }
        
        protected bool IsSafe(int row, int col)
        {
            int i, j;
 
            /* Check this row on left side */
            for (i = 0; i < col; i++)
                if (array[row,i])
                    return false;
 
            /* Check upper diagonal on left side */
            for (i=row, j=col; i>=0 && j>=0; i--, j--)
                if (array[i,j])
                    return false;
 
            /* Check lower diagonal on left side */
            for (i=row, j=col; j>=0 && i<boardSize; i++, j--)
                if (array[i,j])
                    return false;
 
            return true;
        }

        public string ToString()
        {
            var buff = new StringBuilder();
            for (int row = 0; row < array.GetLength(0); row++)
            {
                for (int col = 0; col < array.GetLength(1); col++)
                {
                    buff.Append((array[row, col] ? "Q" : " ") + " ");
                }

                buff.Append("\n");
            }

            return buff.ToString();
        }

    }

}