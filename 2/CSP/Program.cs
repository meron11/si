﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using CSP.NQueens;
using CSP.Sudoku;
using System.Globalization;

namespace CSP
{
    class Program
    {
        static void Main(string[] args)
        {
            var culture = new CultureInfo("en-US");
            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;
        
            //sudoku();
            nqueens();
           
        }

        public static void nqueens()
        {
            var board = 15;
            var iterations = true;

            Console.WriteLine($"NQUEENS,GridSize={board}");
            var backTimeList = new List<double>();
            var backIter= 0L;
            var fwdTimeList = new List<double>();
            var fwdIter = 0L;
            //test
            for(var i=0; i < 10; i++){
                Stopwatch stopWatch = new Stopwatch();

                var nqback = new NQueensBacktracking(board);
                stopWatch.Start();
                var result = nqback.Solve();
                stopWatch.Stop();
                backIter = nqback.iterations;
                backTimeList.Add(stopWatch.Elapsed.TotalMilliseconds);

                var nqfwd = new NQueensForwardChecking(board);
                stopWatch.Reset();
                stopWatch.Start();
                var fwd = nqfwd.Solve();
                stopWatch.Stop();
                fwdIter = nqfwd.iterations;
                fwdTimeList.Add(stopWatch.Elapsed.TotalMilliseconds);

                if(!nqfwd.ToString().Equals(nqback.ToString()))
                    throw new Exception("Bad result!");
            }
            if(!iterations){
                Console.WriteLine($"Backtracking\t{backTimeList.Average()}");
                Console.WriteLine($"Forward Checking\t{fwdTimeList.Average()}");
            }else
            {
                Console.WriteLine($"Backtracking\t{backIter}");
                Console.WriteLine($"Forward Checking\t{fwdIter}");
            }

        }


        public static void sudoku() 
        {

            //ez
            // int[,] grid = new int[9,9] {
            //     {8, 0, 0, 0, 0, 4, 3, 0, 1},
            //     {9, 3, 5, 0, 0, 8, 4, 0, 0},
            //     {0, 6, 0, 2, 0, 0, 0, 0, 0},
            //     {6, 0, 0, 3, 0, 0, 0, 0, 0},
            //     {0, 7, 0, 0, 1, 0, 0, 4, 0},
            //     {0, 0, 0, 0, 0, 5, 0, 0, 6},
            //     {0, 0, 0, 0, 0, 3, 0, 7, 2},
            //     {0, 0, 9, 7, 0, 0, 8, 3, 4},
            //     {2, 0, 3, 8, 0, 0, 0, 0, 9}
            // };

            //ezz
            // int[,] grid = new int[9,9] {
            //     {8, 0, 0, 0, 6, 4, 3, 0, 1},
            //     {9, 3, 5, 1, 0, 8, 4, 0, 0},
            //     {0, 6, 0, 2, 0, 9, 5, 8, 0},
            //     {6, 0, 0, 3, 0, 7, 2, 0, 0},
            //     {5, 7, 0, 0, 1, 0, 9, 4, 0},
            //     {3, 0, 0, 0, 0, 5, 7, 0, 6},
            //     {0, 0, 0, 0, 0, 3, 0, 7, 2},
            //     {0, 0, 9, 7, 0, 0, 8, 3, 4},
            //     {2, 4, 3, 8, 0, 0, 0, 0, 9}
            // };


            //killer sudoku
            //int[,] grid = new int[9,9] {
            //    {0, 0, 0, 0, 0, 0, 0, 0, 0},
            //    {0, 0, 0, 0, 0, 3, 0, 8, 5},
            //    {0, 0, 1, 0, 2, 0, 0, 0, 0},
            //    {0, 0, 0, 5, 0, 7, 0, 0, 0},
            //    {0, 0, 4, 0, 0, 0, 1, 0, 0},
            //    {0, 9, 0, 0, 0, 0, 0, 0, 0},
            //    {5, 0, 0, 0, 0, 0, 0, 7, 3},
            //    {0, 0, 2, 0, 1, 0, 0, 0, 0},
            //    {0, 0, 0, 0, 4, 0, 0, 0, 9}
            //};

            // //medium
            //int[,] grid = new int[9, 9] {
            //     {6, 0, 0, 0, 0, 0, 0, 0, 0},
            //     {0, 0, 0, 0, 1, 0, 6, 0, 0},
            //     {0, 7, 9, 0, 0, 6, 0, 0, 0},
            //     {0, 0, 2, 0, 0, 0, 0, 7, 8},
            //     {0, 9, 0, 0, 0, 0, 0, 5, 0},
            //     {0, 3, 0, 4, 5, 0, 9, 0, 0},
            //     {9, 0, 0, 0, 8, 2, 5, 6, 0},
            //     {2, 0, 0, 7, 0, 0, 0, 8, 0},
            //     {3, 0, 0, 0, 0, 0, 0, 0, 1}
            // };

            //  //fc szybsze
            int[,] grid = new int[9, 9] {
                 {1, 0, 0, 3, 0, 0, 0, 0, 8},
                 {0, 0, 0, 0, 0, 0, 0, 3, 0},
                 {0, 0, 0, 5, 0, 0, 0, 0, 2},
                 {0, 0, 0, 0, 0, 8, 0, 0, 0},
                 {3, 0, 5, 0, 0, 0, 6, 0, 4},
                 {0, 0, 0, 1, 0, 0, 0, 0, 0},
                 {0, 0, 0, 0, 0, 5, 2, 0, 0},
                 {0, 0, 0, 0, 3, 0, 0, 0, 0},
                 {7, 0, 0, 0, 0, 2, 0, 8, 9}
             };

            var zeros = 0;
            var filled = 0;
            for(var row = 0; row < grid.GetLength(0);row++)
                for(var col = 0; col < grid.GetLength(0);col++)
                    if(grid[row,col] == 0)
                        zeros++;
                    else
                        filled++;

            var iterations = false;

            Console.WriteLine($"Sudoku,GridSize={grid.GetLength(0)},Empty={zeros},NonEmpty={filled}");
            var backTimeList = new List<double>();
            var backIter= 0L;
            var fwdTimeList = new List<double>();
            var fwdIter = 0L;
            //test
            for(var i=0; i < 1; i++){
               Stopwatch stopWatch = new Stopwatch();

                var sudokuBack = new SudokuBackTracking((int[,])grid.Clone());
                stopWatch.Start();
                var result = sudokuBack.Solve();
                stopWatch.Stop();
                backIter = sudokuBack.iterations;
                backTimeList.Add(stopWatch.Elapsed.TotalMilliseconds);

                var sudokuForward = new SudokuForwardChecking((int[,])grid.Clone());
                stopWatch.Restart();
                var fwd = sudokuForward.Solve();
                stopWatch.Stop();
                fwdIter = sudokuForward.iterations;
               fwdTimeList.Add(stopWatch.Elapsed.TotalMilliseconds);
            }
            if(!iterations){
                Console.WriteLine($"Backtracking\t{backTimeList.Average()}");
                Console.WriteLine($"Forward Checking\t{fwdTimeList.Average()}");
            }else
            {
                Console.WriteLine($"Backtracking\t{backIter}");
                Console.WriteLine($"Forward Checking\t{fwdIter}");
            }

        }
    }
}